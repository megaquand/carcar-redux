from django.urls import path
from .views import (
    api_list_salespeople,
    api_detail_salespeople,
    api_list_customers,
    api_detail_customers,
    api_list_sales,
    api_detail_sales,
)


urlpatterns = [
    path("salespeople/", api_list_salespeople,
         name="api_list_salespeople"),
    path("salespeoeple/<int:pk>/", api_detail_salespeople,
         name="api_detail_salespeople"),
    path("customers/", api_list_customers,
         name="api_list_customers"),
    path("customers/<int:pk>/", api_detail_customers,
         name="api_detail_customer"),
    path("sales/", api_list_sales,
         name="api_list_sales"),
    path("sales/<int:pk>/", api_detail_sales,
         name="api_detail_sales"),
]
