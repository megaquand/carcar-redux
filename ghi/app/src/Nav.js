import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" to="inventory/">Inventory List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="inventory/create/">Add Car to Inventory</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="manufacturers/">Manufacturer List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="manufacturers/create/">Create Manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="models/">Model List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="models/create/">Create Model</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="technicians/">Technician List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="technicians/create">Add Technician</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="appointments">Appointment List</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="appointments/create">Add Appointment</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="appointments/history">Appointment History</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link" aria-current="page" to="/salesperson/">
              List of Salespeople
            </NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link" aria-current="page" to="/salesperson/records">
              Saleperson's Record
            </NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link" aria-current="page" to="/customer/">
              List of Customers
            </NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link" aria-current="page" to="/customer/create">
              New Customer
            </NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link" aria-current="page" to="/sale/">
              List of Sales
            </NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link" aria-current="page" to="/sale/create">
              New Sale
            </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
