import React, { useState, useEffect } from 'react';

function TechnicianList() {
    const [technicians, setTechnicians] = useState([]);

    useEffect(() => {
        const fetchTechnicians = async () => {
            try {
                const response = await fetch('http://localhost:8080/api/technicians/');
                if (!response.ok) {
                    throw new Error('Failed to fetch technicians');
                }
                const data = await response.json();
                setTechnicians(data.technicians);
            } catch (error) {
                console.error(error);
            }
        };
        fetchTechnicians();
    }, []);

    return (
        <div>
            <h2>Technicians</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.map(technician => (
                        <tr key={technician.employee_id}>
                            <td>{technician.first_name}</td>
                            <td>{technician.last_name}</td>
                            <td>{technician.employee_id}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default TechnicianList;
