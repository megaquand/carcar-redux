import React, { useState, useEffect } from 'react';

function TechnicianCreate() {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [employeeId, setEmployeeId] = useState('')

    const handleFirstNameChange = (event) => {
        const value = event.target.value
        setFirstName(value)
    }
    const handleLastNameChange = (event) => {
        const value = event.target.value
        setLastName(value)
    }
    const handleEmployeeIdChange = (event) => {
        const value = event.target.value
        setEmployeeId(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.first_name = firstName
        data.last_name = lastName
        data.employee_id = employeeId

        const TechURL = "http://localhost:8080/api/technicians/create"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        }

        const response = await fetch(TechURL, fetchConfig)
        if (response.ok) {
            const newTech = await response.json()
            setFirstName('')
            setLastName('')
            setEmployeeId('')
        }
    }
    useEffect(() => {
    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
            <h2>Create Technician</h2>
            <form onSubmit={handleSubmit} id="create-tech-form">
                <div className="form-floating mb-3">
                    <input required type="text" placeholder="First name" className="form-control" id="first_name" name="first_name" value={firstName} onChange={handleFirstNameChange} />
                    <label htmlFor="first_name">First Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input required type="text" placeholder="Last Name" className="form-control" id="last_name" name="last_name" value={lastName} onChange={handleLastNameChange} />
                    <label htmlFor="last_name">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input required type="number" placeholder="Employee ID" className="form-control" id="employee_id" name="employee_id" value={employeeId} onChange={handleEmployeeIdChange} />
                    <label htmlFor="employee_id">Employee ID</label>
                </div>
                <button type="submit" className="btn btn-primary">Create</button>
            </form>
            </div>
            </div>
        </div>
    );
}

export default TechnicianCreate;
