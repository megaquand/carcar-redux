import React, { useState, useEffect } from 'react';

function formatDateTime(dateTimeString) {
    const dateTime = new Date(dateTimeString);
    const date = dateTime.toLocaleDateString('en-US');
    const time = dateTime.toLocaleTimeString('en-US');
    return { date, time };
}

function ServiceList() {
    const [appointments, setAppointments] = useState([]);

    useEffect(() => {
        const fetchAppointments = async () => {
            try {
                const response = await fetch('http://localhost:8080/api/appointments/');
                if (response.ok) {
                    const data = await response.json();
                    setAppointments(data.appointments);
                }
            } catch (error) {
                console.error('Error fetching appointments:', error);
            }
        };

        fetchAppointments();
    }, []);

    const cancelAppointment = async (appointmentId) => {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${appointmentId}/cancel`, {
                method: 'PUT',
            });
            if (response.ok) {
                window.location.reload()
            }
        } catch (error) {
            console.error('Error canceling appointment:', error);
        }
    };

    const finishAppointment = async (appointmentId) => {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${appointmentId}/finish`, {
                method: 'PUT',
            });
            if (response.ok) {
                window.location.reload()
        }
    } catch (error) {
            console.error('Error finishing appointment:', error);
        }
    };

    return (
        <div>
            <h2>Service Appointments</h2>
            <div className="mt-4">
                <h3>All Appointments:</h3>
                {appointments.length === 0 ? (
                    <p>No appointments found</p>
                ) : (
                    <table className="table">
                        <thead>
                            <tr>
                                <th>VIN</th>
                                <th>VIP</th>
                                <th>Customer</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Technician</th>
                                <th>Reason</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {appointments.map(appointment => {
                                const { date, time } = formatDateTime(appointment.date_time);
                                return (
                                    <tr key={appointment.id}>
                                        <td>{appointment.vin}</td>
                                        <td>{appointment.vip ? 'Yes' : 'No'}</td>
                                        <td>{appointment.customer}</td>
                                        <td>{date}</td>
                                        <td>{time}</td>
                                        <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                        <td>{appointment.reason}</td>
                                        <td>{appointment.status}</td>
                                        <td>
                                            {appointment.status === 'scheduled' && (
                                                <>
                                                    <button className="btn btn-danger" onClick={() => cancelAppointment(appointment.id)}>Cancel</button>
                                                    <button className="btn btn-success" onClick={() => finishAppointment(appointment.id)}>Finish</button>
                                                </>
                                            )}
                                        </td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                )}
            </div>
        </div>
    );
}

export default ServiceList;
