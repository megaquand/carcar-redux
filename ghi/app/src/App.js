import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import SalespersonRecords from "./sales/SalespersonRecords";
import SalespersonList from "./sales/SalespersonList";
import SalespersonAdd from "./sales/SalespersonAdd";
import CustomersList from "./sales/CustomersList";
import CustomerAdd from "./sales/CustomerAdd";
import SalesList from "./sales/SalesList";
import SaleAdd from "./sales/RecordANewSale";
import TechnicianList from './service/TechList';
import TechnicianCreate from './service/TechForm';
import ServiceAppointment from './service/ServiceAppointment';
import ServiceHistory from './service/ServiceHistory';
import ServiceList from './service/ServiceList';
import ListManufacturers from './inventory/ListManufacturers'
import ListModels from './inventory/ListModels';
import ListInventory from './inventory/ListInventory';
import CreateManufacturer from './inventory/CreateManufacturer';
import CreateModel from './inventory/CreateModel';
import CreateInventory from './inventory/CreateInventory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="technicians/" element={<TechnicianList />} />
          <Route path="technicians/create/" element={<TechnicianCreate />} />
          <Route path="salesperson/" element={<SalespersonList />} />
          <Route path="salesperson/create/" element={<SalespersonAdd />} />
          <Route path="salesperson/records/" element={<SalespersonRecords />} />
          <Route path="customer/" element={<CustomersList />} />
          <Route path="customer/create/" element={<CustomerAdd />} />
          <Route path="sale/" element={<SalesList />} />
          <Route path="sale/create/" element={<SaleAdd />} /> */
          <Route path="appointments/create/" element={<ServiceAppointment />} />
          <Route path="appointments/history/" element={<ServiceHistory />} />
          <Route path="appointments/" element={<ServiceList />} />
          <Route path="manufacturers/" element={<ListManufacturers />} />
          <Route path="models/" element={<ListModels />} />
          <Route path="inventory/" element={<ListInventory />} />
          <Route path="manufacturers/create/" element={<CreateManufacturer />} />
          <Route path="models/create/" element={<CreateModel />} />
          <Route path="inventory/create/" element={<CreateInventory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
