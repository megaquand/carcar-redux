import React, { useState, useEffect } from 'react';

function AutomobileCreate() {
    const [color, setColor] = useState('')
    const [year, setYear] = useState('')
    const [vin, setVIN] = useState('')
    const [sold, setSold] = useState('')


    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }
    const handleYearChange = (event) => {
        const value = event.target.value
        setYear(value)
    }
    const handleVinChange = (event) => {
        const value = event.target.value
        setVIN(value)
    }
    const handleSoldChange = (event) => {
        const value = event.target.value
        setSold(value)
    }


    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.color = color
        data.year = year
        data.vin = vin
        data.sold = sold

        const AutomobileURL = "http://localhost:8100/api/automobiles/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        }

        const response = await fetch(AutomobileURL, fetchConfig)
        if (response.ok) {
            const newAutomobile = await response.json()
            console.log(newAutomobile)
            setColor('')
            setYear('')
            setVIN('')
            setSold('')
        }
    }
    useEffect(() => {
    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
            <h2>Create Automobile</h2>
            <form onSubmit={handleSubmit} id="create-tech-form">
                <div className="form-floating mb-3">
                    <input required type="text" placeholder="Color" className="form-control" id="color" name="color" value={color} onChange={handleColorChange} />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input required type="text" placeholder="Year" className="form-control" id="year" name="year" value={year} onChange={handleYearChange} />
                    <label htmlFor="year">Year</label>
                </div>
                <div className="form-floating mb-3">
                    <input required type="text" placeholder="VIN" className="form-control" id="vin" name="vin" value={vin} onChange={handleVinChange} />
                    <label htmlFor="vin">VIN</label>
                </div>
                <div className="form-floating mb-3">
                    <input required type="text" placeholder="Sold" className="form-control" id="sold" name="sold" value={sold} onChange={handleSoldChange} />
                    <label htmlFor="sold">Sold</label>
                </div>
                <button type="submit" className="btn btn-primary">Create</button>
            </form>
            </div>
            </div>
        </div>
    );
}

export default AutomobileCreate;
