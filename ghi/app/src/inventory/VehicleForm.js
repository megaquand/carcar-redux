import React, { useState, useEffect } from 'react';

function VehicleCreate() {
    const [name, setName] = useState('')
    const [picture_url, setPictureUrl] = useState('')
    const [manufacturer, setManufacturer] = useState('')



    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }
    const handlePictureUrlChange = (event) => {
        const value = event.target.value
        setPictureUrl(value)
    }
    const handleManufacturerChange = (event) => {
        const value = event.target.value
        setVIN(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.name = name
        data.picture_url = picture_url
        data.manufacturer = manufacturer
        data.sold = sold

        const VehicleURL = "http://localhost:8100/api/vehicles/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        }

        const response = await fetch(VehicleURL, fetchConfig)
        if (response.ok) {
            const newVehicle = await response.json()
            console.log(newVehicle)
            setName('')
            setPictureUrl('')
            setManufacturer('')
            setSold('')
        }
    }
    useEffect(() => {
    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
            <h2>Create Vehicle</h2>
            <form onSubmit={handleSubmit} id="create-tech-form">
                <div className="form-floating mb-3">
                    <input required type="text" placeholder="Name" className="form-control" id="name" name="name" value={name} onChange={handleNameChange} />
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input required type="text" placeholder="picture_url" className="form-control" id="picture_url" name="picture_url" value={picture_url} onChange={handlePictureUrlChange} />
                    <label htmlFor="year">Picture Url</label>
                </div>
                <div className="form-floating mb-3">
                    <input required type="text" placeholder="Manufacturer" className="form-control" id="manufacturer" name="manufacturer" value={manufacturer} onChange={handleManufacturerChange} />
                    <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <button type="submit" className="btn btn-primary">Create a Vehicle</button>
            </form>
            </div>
            </div>
        </div>
    );
}

export default VehicleCreate;
