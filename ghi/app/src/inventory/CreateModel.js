import React, { useState, useEffect } from 'react';

function CreateModel() {
    const [modelName, setModelName] = useState('')
    const [pictureUrl, setPictureUrl] = useState('')
    const [manufacturer, setManufacturer] = useState('')
    const [manufacturers, setManufacturers] = useState([])

    const handleModelNameChange = (event) => {
        const value = event.target.value
        setModelName(value)
    }
    const handlePictureUrlChange = (event) => {
        const value = event.target.value
        setPictureUrl(value)
    }
    const handleManufacturerChange = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }

    const fetchManufacturers = async () => {
        const manuUrl = "http://localhost:8100/api/manufacturers"
        const response = await fetch(manuUrl)
        if (response.ok) {
            const manuData = await response.json()
            setManufacturers(manuData.manufacturers)
        }
    }

    useEffect(() => {
        fetchManufacturers()
    }, [])



    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.name = modelName
        data.picture_url = pictureUrl
        data.manufacturer_id = manufacturer
        const modelURL = "http://localhost:8100/api/models/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        }

        const response = await fetch(modelURL, fetchConfig)
        if (response.ok) {
            const newModelName = await response.json()
            setModelName('')
            setPictureUrl('')
            setManufacturer('')
        }
    }
    useEffect(() => {
    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h2>Create Model</h2>
                    <form onSubmit={handleSubmit} id="create-tech-form">
                        <div className="form-floating mb-3">
                            <input required type="text" placeholder="Model Name" className="form-control" id="name" name="name" value={modelName} onChange={handleModelNameChange} />
                            <label htmlFor="name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input required type="url" placeholder="Picture URL" className="form-control" id="picture_url" name="picture_url" value={pictureUrl} onChange={handlePictureUrlChange} />
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select required name="mfr" id="mfrs" className="form-select" value={manufacturer} onChange={handleManufacturerChange}>
                                <option value="">Select Manufacturer</option>
                                {manufacturers.map(manufacturer => {
                                    return (
                                        <option key={manufacturer.id} value={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CreateModel;
