import React, { useState, useEffect } from "react";

function CustomerAddForm() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [address, setAddress] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [automobile, setAutomobile] = useState("");
  const [formSubmitted, setFormSubmitted] = useState(false);

const handleFirstNameChange = (event) => {
  const value = event.target.value;
  setFirstName(value);
};
const handleLastNameChange = (event) => {
  const value = event.target.value;
  setLastName(value);
};
const handleAddressChange = (event) => {
  const value = event.target.value;
  setAddress(value);
};
const handlePhoneNumberChange = (event) => {
  const value = event.target.value;
  setPhoneNumber(value);
};
const handleSubmit = async (event) => {
  event.preventDefault();
  const data = {};
  data.first_name = firstName;
  data.last_name = lastName;
  data.address = address;
  data.phone_number = phoneNumber;

  const CustomerUrl = "http://localhost:8090/api/customers/";
  const fetchConfig = {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
    },
  };
  const response = await fetch(CustomerUrl, fetchConfig);
  if (response.ok) {
    const newCustomer = await response.json();
    setFirstName("");
    setLastName("");
    setAddress("");
    setPhoneNumber("");
    setFormSubmitted(true);
  }
};
useEffect(() => {
}, []);

return (
  <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>New Customer</h1>
        <form
          onSubmit={handleSubmit}
          id="create-customer-form"
        >
          <div className="form-floating mb-3">
            <input
              onChange={handleFirstNameChange}
              value={firstName}
              placeholder="first_name"
              required
              type="text"
              first_name="first_name"
              id="first_name"
              className="form-control"
            />
            <label htmlFor="first_name">First Name</label>
          </div>
          <div className="form-floating mb-3">
            <input
              onChange={handleLastNameChange}
              value={lastName}
              placeholder="last_name"
              required
              type="text"
              name="last_name"
              id="last_name"
              className="form-control"
            />
            <label htmlFor="name">Last Name</label>
          </div>
          <div className="form-floating mb-3">
            <input
              onChange={handleAddressChange}
              value={address}
              placeholder="address"
              required
              type="text"
              address="address"
              id="address"
              className="form-control"
            />
            <label htmlFor="address">Address</label>
          </div>
          <div className="form-floating mb-3">
            <input
              onChange={handlePhoneNumberChange}
              value={phoneNumber}
              placeholder="phone_number"
              required
              type="text"
              phone_number="phone_number"
              id="phone_number"
              className="form-control"
            />
            <label htmlFor="phone_number">Phone Number</label>
          </div>
          <button className="btn btn-success">Create</button>
        </form>
          New Customer Added
        </div>
      </div>
    </div>
)};

export default CustomerAddForm;
