from django.urls import path
from .views import (
    technician_list,
    technician_detail,
    appointment_cancel,
    appointment_detail,
    appointment_finish,
    appointment_list,
    technician_create,
    technician_edit,
    appointment_create,
    technician_delete,
    appointment_delete,
)


urlpatterns = [
    path("technicians/", technician_list, name="technician_list"),
    path("technicians/<int:id>", technician_detail, name="technician_detail"),
    path("technicians/create", technician_create, name="technician_create"),
    path("technicians/edit/<int:id>", technician_edit, name="technician_edit"),
    path("technicians/delete/<int:id>", technician_delete, name="technician_delete"),
    path("appointments/", appointment_list, name="appointment_list"),
    path("appointments/create", appointment_create, name="appointment_create"),
    path("appointments/<int:id>", appointment_detail, name="appointment_detail"),
    path("appointments/<int:id>/cancel", appointment_cancel, name="appointment_cancel"),
    path("appointments/<int:id>/finish", appointment_finish, name="appointment_finish"),
    path("appointments/delete/<int:id>", appointment_delete, name="appointment_delete"),
]
