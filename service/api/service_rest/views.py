from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .encoders import TechnicianEncoder, AppointmentDetailEncoder
from .models import Technician, Appointment, AutomobileVO
import json

@require_http_methods(["GET"])
def technician_list(request): # working
    technicians = Technician.objects.all()
    return JsonResponse(
        {"technicians": technicians},
        encoder=TechnicianEncoder)

@require_http_methods(["GET"])
def technician_detail(request, id): # working
    technician = Technician.objects.get(id=id)
    return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
            )

@require_http_methods(["POST"])
def technician_create(request): # working
        try:
            data = json.loads(request.body)
            technician = Technician.objects.create(**data)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"technician": "Failed to create a technician, check details and try again. (Needs first_name, last_name, employee_id)"}
            )
            response.status_code = 400
            return response

@require_http_methods(["PUT"])
def technician_edit(request, id): # working
        data = json.loads(request.body)
        try:
            technician = Technician.objects.filter(id=id)
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Technician does not exist"})
            response.status_code = 404
            return response
        technician.update(**data)
        return JsonResponse(
                Technician.objects.get(id=id),
                encoder=TechnicianEncoder,
                safe=False,
        )
@require_http_methods(["DELETE"])
def technician_delete(request, id): #doesnotexists broken
    technician = Technician.objects.get(id=id)
    count, _ = Technician.objects.filter(id=id).delete()
    try:
        technician.delete()
        return JsonResponse(
            {"Technician deleted": count > 0}
        )
    except technician.DoesNotExist:
        response = JsonResponse({"message": "Technician does not exist"})
        response.status_code = 404
        return response


@require_http_methods(["GET"])
def appointment_list(request): #works
    appointment = Appointment.objects.all()
    return JsonResponse(
        {"appointments": appointment},
        encoder=AppointmentDetailEncoder,
        status=200
    )

@require_http_methods(["POST"])
def appointment_create(request): #works
    data = json.loads(request.body)
    try:
        try:
            technician_id = data["technician"]
            technician = Technician.objects.get(id=technician_id)
            data["technician"] = technician
        except Technician.DoesNotExist: # works
            return JsonResponse(
                {"message": "Invalid Technician"},
                status=400,
            )
        for car in AutomobileVO.objects.filter(sold=True): #likely working, still need to test
            if car.vin == data["vin"]:
                data["vip"] = True

        appointment = Appointment.objects.create(**data)
        appointment.status = "scheduled"
        appointment.save()
        return JsonResponse(
            {"appointments": appointment},
            encoder=AppointmentDetailEncoder,
            safe=False,
            status=200)
    except: #datetime empty catches, need reason customer and vin validation
        response = JsonResponse(
            {"appointments": "Failed to make appointment, please check data and try again."}
        )
        response.status = 400
        return response


@require_http_methods(["GET"])
def appointment_detail(request, id): #working
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["DELETE"])
def appointment_delete(request, id): #working
    count, _ = Appointment.objects.filter(id=id).delete()
    if count > 0:
        return JsonResponse(
            {"Appointment has been deleted" : count > 0}
        )
    else:
        response = JsonResponse({"message": "Appointment does not exist"})
        response.status_code = 404
        return response


def appointment_cancel(request, id):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            if appointment.status == "canceled":
                return JsonResponse({"message": "Appointment has already been canceled. Make sure you're cancelling the correct appointment"})
            else:
                appointment.status = "canceled"
                appointment.save()
                response = JsonResponse({"status": "canceled"})
                response.status = 200
                return response
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response


def appointment_finish(request, id):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            if appointment.status == "finished":
                return JsonResponse({"message": "Appointment has already been finished. Make sure you're finishing the correct appointment"})
            else:
                appointment.status = "finished"
                appointment.save()
                return JsonResponse({"status": "finished"})
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
