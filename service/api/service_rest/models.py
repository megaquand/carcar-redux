from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}, ID: {self.employee_id}"


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=20, choices=(
        ("scheduled", "Scheduled"),
        ("canceled", "Canceled"),
        ("finished", "Finished"),
    ))
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        related_name="tech",
        on_delete=models.CASCADE
        )
    vip = models.BooleanField(default=False)
    def __str__(self):
        return f"{self.customer} - {self.date_time}"
